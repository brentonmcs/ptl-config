(function(config) {
    "use strict";

    config.queueUri = null;
    config.queueName = "PickTheLadder";
    config.mongoUri = "mongodb://localhost:27017/PickTheLadder";

})(module.exports);
